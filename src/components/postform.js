import React,{Component} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-type'
import {createpost, fetchPosts} from "../actions/postActions";

class PostForm extends Component {
    constructor(props) {
        super()
        this.state = {
            title: null,
            body: null
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    onSubmit(e) {
        e.preventDefault()
        const post = {
            title: this.state.title,
            body: this.state.body
        }
        //call action
        this.props.createpost()
    }
    render() {

        return (
            <div>
                <h1>Add post</h1>
                <form onSubmit={this.onSubmit}>
                    <div>
                    <label>Title</label><br />
                        <input type="text" onChange={this.onChange} name="title" value={this.state.title} />
                    </div>

                    <div>
                        <label>body</label><br />
                        <textarea name="body" onChange={this.onChange} value={this.state.body}  />
                    </div>

                    <br />

                    <button type="submit">add</button>
                </form>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    post: state.posts.item
})
export default connect(mapStateToProps,{createpost})(PostForm)